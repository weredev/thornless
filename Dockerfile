FROM registry.gitlab.com/weredev/base-containers/dotnet-build:latest AS build-env

WORKDIR /src
ADD . .
WORKDIR "/src/Thornless.UI.Web"
RUN dotnet build "Thornless.UI.Web.csproj" -c Release -o /app/build
RUN dotnet publish "Thornless.UI.Web.csproj" --runtime linux-musl-x64 -c Release -o /app/publish


FROM registry.gitlab.com/weredev/base-containers/dotnet-run:latest AS run-env
COPY --from=build-env /app/publish .

USER root
RUN apk add --no-cache --update icu-libs
RUN chown appuser /app/data
USER appuser

EXPOSE 5000

ENTRYPOINT [ "dotnet", "Thornless.UI.Web.dll" ]