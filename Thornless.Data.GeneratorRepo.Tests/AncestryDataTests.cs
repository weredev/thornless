using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using NUnit.Framework.Legacy;
using Thornless.Data.GeneratorRepo.DataModels;

namespace Thornless.Data.GeneratorRepo.Tests
{
    [TestFixture]
    public class AncestryDataTests
    {
        [Test]
        public async Task CheckAncestriesForValidData()
        {
            var database = new GeneratorContext(new DbContextOptions<GeneratorContext>());
            var data = await database.CharacterAncestries
                                .Include(x => x.Options)
                                    .ThenInclude(x => x.SegmentGroups)
                                .Include(x => x.NameParts)
                                .ToListAsync();
            
            foreach (var ancestry in data)
            {
                VerifyCharacterAncestryNameParts(ancestry.NameParts);
                VerifyCharacterAncestryOptions(ancestry.Options);
            }
        }

        private static void VerifyCharacterAncestryNameParts(List<CharacterAncestryNamePartDto> nameParts)
        {
            foreach (var namePart in nameParts)
            {
                ClassicAssert.IsTrue(IsValidJson(namePart!.NamePartsJson), $"Invalid NamePartsJson in CharacterAncestryNamePart {namePart?.Id}.");
                if (namePart!.NameMeaningsJson != null)
                    ClassicAssert.IsTrue(IsValidJson(namePart.NameMeaningsJson), $"Invalid NameMeaningsJson in CharacterAncestryNamePart {namePart.Id}.");
            }
        }

        private static void VerifyCharacterAncestryOptions(List<CharacterAncestryOptionDto> options)
        {
            ClassicAssert.AreEqual(options.Count(), options.Select(x => x.Code).Distinct().Count(), $"Duplicate CharacterAncestryOption.Code for CharacterAncestry {options[0].CharacterAncestryId}.");
            foreach (var option in options)
            {
                ClassicAssert.IsTrue(IsValidJson(option.NamePartSeperatorJson), $"Invalid NamePartSeperatorJson in CharacterAncestryOption {option.Id}");
                ClassicAssert.GreaterOrEqual(option.SeperatorChancePercentage, 0, $"Invalid SeperatorChancePercentage in CharacterAncestryOption {option.Id}");
                ClassicAssert.LessOrEqual(option.SeperatorChancePercentage, 100, $"Invalid SeperatorChancePercentage in CharacterAncestryOption {option.Id}");
                VerifyCharacterAncestrySegmentGroups(option.SegmentGroups);
            }
        }

        private static void VerifyCharacterAncestrySegmentGroups(List<CharacterAncestrySegmentGroupDto> groups)
        {
            foreach (var group in groups)
            {
                ClassicAssert.IsTrue(IsValidJson(group.NameSegmentCodesJson), $"Invalid NameSegmentCodesJson in CharacterAncestrySegment {group.Id}.");
            }
        }

        private static bool IsValidJson(string? value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                    return false;
                ParseJsonArray(value);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static string[] ParseJsonArray(string s)
        {
            return JsonSerializer.Deserialize<string[]>(s) ?? Array.Empty<string>();
        }
    }
}
